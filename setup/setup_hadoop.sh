#!/bin/bash

PIUSER="bm"
HEAD_NODE="192.168.1.11"
HADOOP_PATH="/home/$PIUSER/rpiclust/tests/hadoop"
echo $HADOOP_PATH

#first check if root
if [ "$(whoami)" != "root" ]; then
  echo "You must run this as root."
  exit 1
fi

#sudo -u $PIUSER rsync -r $PIUSER@$HEAD_NODE:/usr/local/hadoop /tmp
#mv /tmp/hadoop /usr/local
addgroup hadoop
adduser bm hadoop
adduser bm sudo
chown -R bm $HADOOP_PATH
mkdir -p /fs/hadoop/tmp
chown bm:hadoop /fs/hadoop/tmp
chmod 750 /fs/hadoop/tmp/
sudo -u $PIUSER /$HADOOP_PATH/bin/hadoop namenode -format
#rm -fr /fs/hadoop/tmp/*;/usr/local/hadoop/bin/hadoop namenode -format
# hadoop jar $HADOOP_INSTALL/hadoop-examples-1.2.1.jar randomwriter -Dtest.randomwriter.maps_per_host=2 random-data2
#mkdir -p /fs/hadoop/tmp; chown bm:hadoop /fs/hadoop/tmp; chmod 750 /fs/hadoop/tmp/; sudo -u bm /usr/local/hadoop/bin/hadoop namenode -format
  
echo 'export JAVA_HOME=/usr/lib/jvm/java-6-openjdk-armhf' >> /home/$PIUSER/.bashrc
echo 'export HADOOP_INSTALL=/home/bm/rpiclust/tests/hadoop' >> /home/$PIUSER/.bashrc
echo 'export PATH=$PATH:$HADOOP_INSTALL/bin' >> /home/$PIUSER/.bashrc

#reboot
