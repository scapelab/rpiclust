#!/bin/bash
rpi_home=/home/bm/rpiclust

echo "starting servo daemon..."
$rpi_home/bin/PIGPIO/pigpiod
sleep 5
$rpi_home/bin/PIGPIO/pigs PFS 7 50
$rpi_home/bin/PIGPIO/pigs PRS 7 4000

echo "moving servo"
# move the arm, for sanity
$rpi_home/bin/PIGPIO/pigs PWM 7 400
sleep 1
$rpi_home/bin/PIGPIO/pigs PWM 7 200
sleep 1
$rpi_home/bin/PIGPIO/pigs PWM 7 400
 
