#!/bin/bash

#first check if root
if [ "$(whoami)" != "root" ]; then
  echo "You must run this as root."
  exit 1
fi

#default location of id file
idDir=/boot/id

#make sure we have the ID of the new PI
if [ ! -z "$1" ]
then
  PI_ID=$1
  echo ID read from the command line as $PI_ID
elif [ -f $idDir ]
then
  PI_ID=$(<$idDir)
  echo id read from local file as $PI_ID
else
  echo Enter the raspberry pi ID to continue
  read PI_ID
fi

#CONFIG
PI_IP="192.168.1.$PI_ID"
PI_HOSTNAME="rp$PI_ID.rpiclust.com"
PI_GATEWAY="192.168.1.100"
PI_DNS="192.168.1.100"
PIUSER="bm"

echo $PI_ID
echo $PI_IP
echo $PI_HOSTNAME

#update hostname
echo $PI_HOSTNAME > /etc/hostname
hostname $PI_HOSTNAME

#update resolv file
echo "options timeout:1" > /etc/resolv.conf
echo "nameserver $PI_DNS" >> /etc/resolv.conf
echo "nameserver 8.8.8.8" >> /etc/resolv.conf

#update hosts file
echo "127.0.0.1       localhost.localdomain localhost" > /etc/hosts

#update networks file
echo "auto lo eth0" > /etc/network/interfaces
echo "iface lo inet loopback" >> /etc/network/interfaces
echo "iface eth0 inet static" >> /etc/network/interfaces
echo "address $PI_IP" >> /etc/network/interfaces
echo "netmask 255.255.255.0" >> /etc/network/interfaces
echo "gateway $PI_GATEWAY" >> /etc/network/interfaces

#restart network interface to update
ifdown eth0
ifup eth0
