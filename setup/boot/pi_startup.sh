#!/bin/bash

# have the pi generate its id very time it boots up
/boot/setup_id.sh

# start up the pig deamon
/home/bm/rpiclust/setup/start_pigd.sh

# start the udp listener
sudo -u bm `source /home/bm/.bashrc;/home/bm/rpiclust/bin/netlistener.py` &

# start the hearbeat
sudo -u bm `source /home/bm/.bashrc;/home/bm/rpiclust/bin/heartbeat.py` &
