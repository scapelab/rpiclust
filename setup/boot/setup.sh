#!/bin/bash

#first check if root
if [ "$(whoami)" != "root" ]; then
  echo "You must run this as root."
  exit 1
fi

#setup the id of this new PI to be able to reach the webz
/boot/setup_id.sh $1
PIUSER="bm"

#install required software
apt-get -y update
apt-get -y install vim
apt-get -y install libcr-dev mpich2 mpich2-doc rsync
apt-get -y install build-essential gfortran libgfortran3
apt-get -y install openjdk-6-jre openjdk-6-jdk openjdk-6-jre-headless
apt-get -y install git-core
apt-get -y install expect
apt-get -y install python2.7
apt-get -y install python-psutil
apt-get -y install sysstat

#create user
useradd -m $PIUSER
echo "$PIUSER:bm" | chpasswd

#create public ssh key for that user
echo -e  'n\n' | sudo -u $PIUSER ssh-keygen -f /home/$PIUSER/.ssh/id_rsa -t rsa -N ''
echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config
/boot/setup_ssh_config.sh

#create folder where all the tests will go in
mkdir /home/$PIUSER/rpiclust
git clone https://factoryEquipment@bitbucket.org/factoryEquipment/rpiclust.git /home/$PIUSER/rpiclust
chown -R $PIUSER:$PIUSER /home/$PIUSER/rpiclust
echo 'export PATH=$PATH:/home/bm/rpiclust/bin' >> /home/$PIUSER/.bashrc
echo 'export RPICLUST_HOME=/home/bm/rpiclust' >> /home/$PIUSER/.bashrc
echo 'export PATH=$PATH:$RPICLUST_HOME/bin' >> /home/$PIUSER/.bashrc

#add rpiclust pi startup script to rc.local
cp /etc/rc.local /etc/bu_rc.local
sed  's/^exit 0/\/boot\/pi_startup.sh \n exit 0 /' /etc/bu_rc.local > /etc/rc.local

#reboot
