#define CLASS 'W'
/*
   This file is generated automatically by the setparams utility.
   It sets the number of processors and the class of the NPB
   in this directory. Do not modify it by hand.   */
   
#define COMPILETIME "01 Nov 2013"
#define NPBVERSION "3.3"
#define CC "mpicc"
#define CFLAGS "-O"
#define CLINK "$(CC)"
#define CLINKFLAGS "-O"
#define C_LIB "-lm"
#define C_INC "(none)"
