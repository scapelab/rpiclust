#define CLASS 'A'
#define NUM_PROCS 8
/*
   This file is generated automatically by the setparams utility.
   It sets the number of processors and the class of the NPB
   in this directory. Do not modify it by hand.   */
   
#define COMPILETIME "01 Nov 2013"
#define NPBVERSION "3.3"
#define MPICC "mpicc"
#define CFLAGS "-O"
#define CLINK "$(MPICC)"
#define CLINKFLAGS "-O"
#define CMPI_LIB "-L/usr/local/lib -lmpi"
#define CMPI_INC "-I/usr/local/include"
