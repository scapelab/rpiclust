===========================================
Building and running Sweep3D for Zia
===========================================
-------------------------------------------
LANL Performance and Architecture Lab (PAL)
-------------------------------------------


1. Compilation
--------------

This version of Sweep3D uses the GNU Autotools for configuration.  A
typical configuration command -- to run from the ``sweep3d-2.2d``
directory -- looks like this::

    ./configure F77=mpif77 FFLAGS="-g -O2"

(Run ``./configure --help`` to see a list of supported options.)

After Sweep3D is configured, compile it by running ``make``.


2. Execution
------------

Edit the ``runsweep`` Perl script.  Define the ``@proclist`` variable
to support the maximum number of processes your system can
support.  (Use all CPU cores if possible.)  Define the
``$mpirun_template`` variable as the command to use to launch an MPI
job.  In ``$mpirun_template``, the ``%d`` will be replaced by each
number of processes in turn.

The script will skip runs that have valid output in the sweep3d-results
directory

Execute ``runsweep`` from the ``sweep3d-2.2d`` directory.  For each
set of program parameters, ``runsweep`` generates an ``input`` file,
launches the ``sweep`` executable, and writes the results into a
``sweep3d-results`` directory.  After all runs have finished,
``runsweep`` archives the contents of ``sweep3d-results`` into
``sweep3d-results.tar.gz``.


3. Submitting the results
-------------------------

The ``sweep3d-results.tar.gz`` file should be returned to LANS




