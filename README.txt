The following are steps to set up the pi environment

Use a image writer to write an image to an sd card.

1. Boot up the Pi.
2. Once booted you should get the option to resize the sd card. Resize it.
   Make sure the pi has access to the internet as the next step will require it to pull from repo's
3. Once the pi starts again, login as root again and run setup/setup.sh <id>
  - The id is the id of the Raspberry pi. Meaning this will be the Raspberry Pi IP Address.
4. After setup is done, you will have an account with username:bm and password:bm
5. Log in to bm for the rest of the commands.
4. Run setup/ssh_cp.sh: this should be run to be able to ssh without passwords into each other. 3dsweep needs this.

To run Sweep3d:


To run Hadoop Sort:

To run Search:
