#!/usr/bin/env python2.7
'''
  Converts x,y coordinates to a Pi id according to the current settings found
  in the settings.json file
'''

import argparse
import os
import sys
from pisettings import PI_SETTINGS

def getId(row, col):
    #get the CLUSTER properties of this setup
    colSize = PI_SETTINGS["CLUSTER"]["COL_SIZE"]
    numPis = PI_SETTINGS["CLUSTER"]["NUM_PIS"]

    id = (int(colSize) * row) + col + 1
    #print("%s, %s, %s" %(row, col, id))
    if row < 0 or col < 0 or id > int(numPis):
        return -1

    return id

def main():
    parser = argparse.ArgumentParser(prog='coordstoid', usage=' %(prog)s [col] [row]')
    parser.add_argument('row', type=int)
    parser.add_argument('col', type=int)
    cmdlnArgs = parser.parse_args()

    if cmdlnArgs.row is None or cmdlnArgs.col is None:
        print("usage: coordstoid [row] [col] ")
        return

    return getId(cmdlnArgs.row, cmdlnArgs.col)

if __name__ == '__main__':
    main()
