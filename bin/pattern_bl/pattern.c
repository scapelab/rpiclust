#include <stdio.h>
#include <stdbool.h>
#include <mpi.h>
#include <unistd.h>

/**
 * Get my id based of my hostname.
 * i.e. rp11.rpiclust.com hostname = 11 id
 */
int getMyId()
{
  char hostname[1024];
  hostname[1023] = '\0';

  gethostname(hostname, 1023);

  char myId[3];
  myId[0] = hostname[2];
  myId[1] = hostname[3];
  myId[2] = '\0';
  printf(myId);
  return atoi(myId);
}

/**
 * calculate the wait time in us based on my id.
 */
long getWaitLengthUs(int myId)
{
  return myId * 400000;//400ms intervals
}

int main (argc, argv)
     int argc;
     char *argv[];
{
  int rank, size;
  int myId = getMyId();
  long sleepTime = getWaitLengthUs(myId);
  bool doMirror = false;

  // do mirror ending as opposed to all just letting go at the end TODO: support more flag
  if ( argc > 1 && strcmp(argv[1], "-m") == 0)
  {
    doMirror = true;
  }

// Start of Editing from Bo Li.
  // Cascade pattern (row-wise).
  if(argc>1 && strcmp(argv[1], "-cr")==0){
    MPI_Init(&argc,&argv);
    MPI_Barrier(MPI_COMM_WORLD);
    sleepTime=((myId-1)/6+1)*400000;
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
  }

  // Cascade pattern (column-wise).
  if(argc>1 && strcmp(argv[1], "-cl")==0){
    MPI_Init(&argc,&argv);
    MPI_Barrier(MPI_COMM_WORLD);
    sleepTime=((myId-1)%6+1)*400000;
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
  }

  // Diagonal patern.
  if(argc>1 && strcmp(argv[1], "-d")==0){
    MPI_Init(&argc,&argv);
    MPI_Barrier(MPI_COMM_WORLD);
    sleepTime=((30-myId)%6 + (30-myId)/6+1)*200000;
    //sleepTime=((myId-1)%6+(myId-1)/6+1)*200000;
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    //MPI_Barrier(MPI_COMM_WORLD);
      //usleep(sleepTime);
      usleep(300000);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
  }

  // Odds-evens pattern.
  if(argc>1 && strcmp(argv[1], "-o")==0){
    MPI_Init(&argc,&argv);
    MPI_Barrier(MPI_COMM_WORLD);
    sleepTime=(myId%2+1)*atoi(argv[2]);
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
  }

  // All the patterns.
  if(argc>1 && strcmp(argv[1], "-a")==0){
    MPI_Init(&argc,&argv);
    MPI_Barrier(MPI_COMM_WORLD);
    // Cascade pattern (row-wise).
    sleepTime=((myId-1)/6+1)*atoi(argv[2]);
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    usleep(1000000);
    
    // Cascade pattern (column-wise).
    sleepTime=((myId-1)%6+1)*atoi(argv[2]);
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    usleep(1000000);

    // Diagonal pattern.
    sleepTime=((myId-1)%6+(myId-1)/6+1)*atoi(argv[2]);
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    usleep(1000000);

    // Diagonal pattern (2nd pattern).
    sleepTime=((myId-1)%6+(myId-1)/6+1)*atoi(argv[2]);
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    //MPI_Barrier(MPI_COMM_WORLD);
      usleep(200000);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
    usleep(1000000);

    // Odds-evens pattern.
    sleepTime=(myId%2+1)*atoi(argv[2]);
    usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
    MPI_Barrier(MPI_COMM_WORLD);
      usleep(sleepTime);
    system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0);
    MPI_Barrier(MPI_COMM_WORLD);

    // Butterfly pattern.

 
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
  }
// End of Editing from Bo Li.

  MPI_Init (&argc, &argv);	/* starts MPI */
  //MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
  //MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */
  MPI_Barrier(MPI_COMM_WORLD);
  usleep(sleepTime);
  system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
  MPI_Barrier(MPI_COMM_WORLD);
  if (doMirror) 
  {
    usleep(sleepTime);
  }
  system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
