#!/usr/bin/env python2.7
'''
  Moves any Pi based on a passed x,y coordinate to a particular angle
'''
import argparse

#TODO: add the ability to send a scpeific pi id number if the user already has it
def movepi(row, col, angle):
    print("moving %s , %s , %s" %(row, col, angle))

    from coordstoid import getId
    id = getId(row, col)

    if id != -1:
      from sendcmd2pi import sendCmd
      cmd = 'moveservo ' + str(angle)
      print (str(id) + " " + cmd)
      sendCmd(id, cmd, 3000)

def main():
    parser = argparse.ArgumentParser(prog='movepi', usage=' %(prog)s [row] [col] [angle(0-100)]')
    parser.add_argument('row', help='row of the target pi', type=int);
    parser.add_argument('col', help='column of the target pi', type=int);
    parser.add_argument('angle', help='angle to move the pi (0-100)', type=int);
    cmdlnArgs = parser.parse_args()

    if cmdlnArgs.row < 0 or cmdlnArgs.col < 0:
      print("Error: row and column arguments should be greater than 0")
      return
    if  cmdlnArgs.angle < 0 or cmdlnArgs.angle > 100:
      print("Error: angle of movement should be between 0 and 100")
      return

    movepi(cmdlnArgs.row, cmdlnArgs.col, cmdlnArgs.angle)

if __name__ == '__main__':
    main()
