#include <stdio.h>
#include <stdbool.h>
#include <mpi.h>
#include <unistd.h>

/**
 * Get my id based of my hostname.
 * i.e. rp11.rpiclust.com hostname = 11 id
 */
int getMyId()
{
  char hostname[1024];
  hostname[1023] = '\0';

  gethostname(hostname, 1023);

  char myId[3];
  myId[0] = hostname[2];
  myId[1] = hostname[3];
  myId[2] = '\0';
  printf(myId);
  return atoi(myId);
}

/**
 * calculate the wait time in us based on my id.
 */
long getWaitLengthUs(int myId)
{
  return myId * 400000;//400ms intervals
}

int main (argc, argv)
     int argc;
     char *argv[];
{
  int rank, size;
  int myId = getMyId();
  long sleepTime = getWaitLengthUs(myId);
  bool doMirror = false;

  // do mirror ending as opposed to all just letting go at the end TODO: support more flag
  if ( argc > 1 && strcmp(argv[1], "-m") == 0)
  {
    doMirror = true;
  }

  MPI_Init (&argc, &argv);	/* starts MPI */
  //MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
  //MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */
  MPI_Barrier(MPI_COMM_WORLD);
  usleep(sleepTime);
  system("/home/bm/rpiclust/bin/moveservo.pl 200", (char *) 0); 
  MPI_Barrier(MPI_COMM_WORLD);
  if (doMirror) 
  {
    usleep(sleepTime);
  }
  system("/home/bm/rpiclust/bin/moveservo.pl 400", (char *) 0); 
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
