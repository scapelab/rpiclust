#include <stdio.h>

#include "pigpio.h"

#define SIGNAL 7


static int deg = 155;
int main()
{
system("stty raw");
if (gpioInitialise()<0) return 1;
gpioSetMode(SIGNAL, PI_OUTPUT);
gpioSetPWMrange(SIGNAL, 4000);
gpioSetPWMfrequency(SIGNAL, 50);
int go = 1;
while(go) { 
gpioPWM(SIGNAL,deg);
char c = getchar();
if(c == 'a') deg-=1;
if(c == 'd') deg+=1;
if(deg < 155)
deg = 155;
if(deg > 441)
deg = 441;
if(c == 'e')
go = 0;
}
system("stty cooked");
return 0;
}

