#!/usr/bin/perl
use Getopt::Long;

# copy cmd line args to use later
my @myArgv = @ARGV;
my (@middle, $forward, $reverse, $hold, $stopPiId);

GetOptions( 'm:i{2}'    => \@middle,
            'f'         => \$forward,
            'r'         => \$reverse,
            'h:i'       => \$hold,
            's:i'       => \$stopPiId);

# get my host name
my $hn = `hostname`;
my @hn_array = split('\.', $hn);
my @ID = split('rp', $hn_array[0]);
my $myPiId = int($ID[1]);

# check if we are doing a middle algorithm
if (scalar(@middle) == 2)
{
  GetOptions('m:i{2}' => \@middle);
  print "i died cause i don't know how to do middle yet @middle\n";
  exit;
}


# determine next pi to poke, and if I need to stopPiId
if ($reverse)
{
  print "stop id is: $myPiId < $stopPiId\n";
  if ($stopPiId && $myPiId < $stopPiId)
  {
    exit;
  }
  $nextPiId = $myPiId - 1;
}
  elsif ($forward)
  {
    print "stop id is: $myPiId > $stopPiId\n";
    if ($stopPiId  && $myPiId > $stopPiId)
    {
      exit;
    }
    $nextPiId = $myPiId + 1;
  } 
    else
    {
      print "No -h or -f argument passed\n";
      exit;
    }

# build the ssh remote commmands
my $nextPiAddress = "bm\@rp$nextPiId.rpiclust.com";
my $nextPiCmd = "ssh $nextPiAddress '~/rpiclust/bin/attn.pl @myArgv'";

# move my pi and execute the commands and wait if necessary
if ($hold)
{
  $nextPiCmd .= " &";
  print "$nextPiCmd\n";
  system($nextPiCmd);
  system("/home/bm/rpiclust/bin/moveservo.pl 200 &");
  sleep($hold);
} 
  else 
  {
    print "$nextPiCmd\n";
    system("/home/bm/rpiclust/bin/moveservo.pl 200 &");
    system($nextPiCmd);
  }
system("/home/bm/rpiclust/bin/moveservo.pl 400 &");
