#include <stdio.h>
#include <mpi.h>
#include <unistd.h>

int main (argc, argv)
     int argc;
     char *argv[];
{
  int rank, size;

  printf( "Hello world serg \n");
  MPI_Init (&argc, &argv);	/* starts MPI */
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
  MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */
  printf( "We have an MPI system of size %d, I am %d\n", size, rank );
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  system("/home/bm/rpiclust/bin/readcpu/readcpu.pl 5 -s "); 
  return 0;
}
