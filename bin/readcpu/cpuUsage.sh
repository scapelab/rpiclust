#!/bin/bash
# top -bn650 -d.2 | grep "Cpu(s)" | sed  "s/.*: *\([0-9.]*\)%* us.*/\1/" > /tmp/fromc

#for sweep
#top -bn650 -d.2 | grep "Cpu(s)" | sed  "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1 }' > /tmp/fromc

#for hadoop
top -bn1950 -d.2 | grep "Cpu(s)" | sed  "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1 }' > /tmp/fromc
