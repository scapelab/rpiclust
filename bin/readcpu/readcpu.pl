#!/usr/bin/perl

my $doServo = 0;
if (defined $ARGV[0] and $ARGV == "-s") 
{
	$doServo = 1;
}

# moving average vars
my @winQueue = qw(0, 0, 0);
my $winSum = 0 * 4;
my $winSize = 3;
my $mvAvg = $winSum / $winSize;

while(1)
{
	# get cpu usage
	my $result = `top -bn2 -d .5 | grep "Cpu(s)" | sed  "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk 'NR==2 '`;
	my @allLoads = split(', *', $result);
	my @idleLoad = split('%', $allLoads[3]);
	my $usage = 100 - $idleLoad[0];

	# update moving average
	my $oldUsage = pop @winQueue;
	unshift(@winQueue, $usage);
	$winSum = $winSum + $usage - $oldUsage;
	$winAvg = $winSum / $winSize;
	
	#print "idle: $idleLoad[0], usage: $usage, avg: $winAvg\n";
	

	if ($doServo == 1) 
	{
		my $step = 400; # set to first step
		if ($winAvg > 18) 
		{
			#calculate the servo step. total of 200 steps.
			$step = int(400 - ($winAvg * 2));
			#print "doing servo studff $step for load $usage\n";
			
		}
		system("/home/bm/rpiclust/bin/moveservo.pl $step");
		#print "Step: $step, Avg: $winAvg, Queue: @winQueue\n";
	}
}
