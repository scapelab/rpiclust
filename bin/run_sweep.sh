#!/bin/bash

CURR_DIR=`pwd`
cd ~/rpiclust/tests/rpi_sweep3d-2.2d/
rm -fr sweep3d-results*
./runsweep3d
cd $CURR_DIR
