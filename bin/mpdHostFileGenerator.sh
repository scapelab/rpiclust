#!/bin/bash

mpd_conf="$RPICLUST_HOME/conf/mpd.hosts"
echo ' '> $mpd_conf
#echo '#generated automaticall by checkalive.sh ' > $mpd_conf
for i in {1..30}
do
    target="rp$i.rpiclust.com"
    zero_loss="0"
    loss=`ping -c1 -w1 $target | grep loss | awk -F' ' '{print $6}'| awk -F'\%' '{print $1}'`
    #echo $loss
    if [ "$loss" = "$zero_loss" ]
    then
        echo $target
        echo $target >> $mpd_conf
    fi
done
    
