#!/usr/bin/env python2.7
'''
  Sends a Pi with the given id the given command and its arguments.
  For example, sendcmd2pi 5 mkdir /home/temp
  will send "mkdir /home/temp" to Pi with id 5 as a command through ssh like so:
    ssh bm@rp5.rpiclust.com: "mkdir /home/temp"
'''
#import argparse
#import os
import sys
from pisettings import PI_SETTINGS
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def sendCmd(targetId, cmd, PORT):
    hostnameFormat = PI_SETTINGS["CLUSTER"]["HOSTNAME_FORMAT"]
    s.sendto(cmd, (hostnameFormat%(targetId),PORT))

def main():
    print("hello world")
    id = sys.argv[1]

    if id == "all":
      PORT = int(sys.argv[2])
      cmd = ' '.join(sys.argv[3:])
      for i in range(1,31):
        print(str(i))
        sendCmd(i, cmd, PORT)
    else:
      PORT = int(PI_SETTINGS["CLUSTER"]["UDP_PORT"])
      cmd = ' '.join(sys.argv[2:])
      sendCmd(int(id), cmd, PORT)

if __name__ == '__main__':
    main()
