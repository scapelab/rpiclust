import json
import os


settingsFile = 'settings.json'
PI_SETTINGS = json.load(open(os.path.join(os.getenv('RPICLUST_HOME'), settingsFile)))
