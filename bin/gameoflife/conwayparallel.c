//Timmy Meyer
//11-6-09
//Conway's Game of Life in serial

#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "mpi.h"

#define XNUMGRID 150
#define YNUMGRID 150



int w = 800, h = 800;

double pi = 3.14159265;
double e = 2.71828183;
int rank, tag=0, size;
MPI_Status status;

//this array stores the rank, the x value and the y value of the asking node.
int xyToCheck[3];

//this 2d array is the gameboard. an 'a' is alive and a 'd' is dead, only for the head node
char matrixOfSpots[XNUMGRID][YNUMGRID];
char newMatrixOfSpots[XNUMGRID][YNUMGRID];
/Users/timmy (old)/Documents/Old/cs/par/5conway/conwayparallel.c

void display(void)
{
    //gl setup stuff
    glClear(GL_COLOR_BUFFER_BIT); // clear the screen
    glColor3f(0.00,0.00,0.00);
    
    xyToCheck[0] = 0;
    xyToCheck[1] = -1;//signal to calculate board
    int iterateOverNodes, numSent = 0;
    for(iterateOverNodes = 1; iterateOverNodes < size; iterateOverNodes++)
    {
	if(iterateOverNodes%2 == 1)
	{
	    numSent++;
	    MPI_Send(&xyToCheck, 3, MPI_INT, iterateOverNodes, tag, MPI_COMM_WORLD);//this only tells the node to calculate
	}
    }
    
    
    int receiveCount = 0;
    char matrixOfSpotsReceived[XNUMGRID][YNUMGRID];
    while(receiveCount < numSent)
    {
		int rankOfMatrixAboutToBeReceived;
		
		MPI_Recv(&rankOfMatrixAboutToBeReceived, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		MPI_Recv(&matrixOfSpotsReceived, XNUMGRID * YNUMGRID, MPI_CHAR, rankOfMatrixAboutToBeReceived, tag, MPI_COMM_WORLD, &status);
		
		int iterateoverReceivedMatrixX, iterateoverReceivedMatrixY;
		for(iterateoverReceivedMatrixY = 0; iterateoverReceivedMatrixY < YNUMGRID; iterateoverReceivedMatrixY++)
			for(iterateoverReceivedMatrixX = XNUMGRID * ((double)rankOfMatrixAboutToBeReceived - 1.0) / (size - 1.0); iterateoverReceivedMatrixX < XNUMGRID * (double)rankOfMatrixAboutToBeReceived / (size - 1.0); iterateoverReceivedMatrixX++)
				matrixOfSpots[iterateoverReceivedMatrixX][iterateoverReceivedMatrixY] = matrixOfSpotsReceived[iterateoverReceivedMatrixX][iterateoverReceivedMatrixY];//copy the matrix segment
		
		receiveCount++;
    }
    
	for(iterateOverNodes = 1; iterateOverNodes < size; iterateOverNodes++)
    {
		if(iterateOverNodes%2 == 0)
		{
			numSent++;
			MPI_Send(&xyToCheck, 3, MPI_INT, iterateOverNodes, tag, MPI_COMM_WORLD);//this only tells the node to calculate
		}
    }
	
    while(receiveCount < numSent)
    {
		int rankOfMatrixAboutToBeReceived;
		
		MPI_Recv(&rankOfMatrixAboutToBeReceived, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		MPI_Recv(&matrixOfSpotsReceived, XNUMGRID * YNUMGRID, MPI_CHAR, rankOfMatrixAboutToBeReceived, tag, MPI_COMM_WORLD, &status);
		
		int iterateoverReceivedMatrixX, iterateoverReceivedMatrixY;
		for(iterateoverReceivedMatrixY = 0; iterateoverReceivedMatrixY < YNUMGRID; iterateoverReceivedMatrixY++)
			for(iterateoverReceivedMatrixX = XNUMGRID * ((double)rankOfMatrixAboutToBeReceived - 1.0) / (size - 1.0); iterateoverReceivedMatrixX < XNUMGRID * (double)rankOfMatrixAboutToBeReceived / (size - 1.0); iterateoverReceivedMatrixX++)
				matrixOfSpots[iterateoverReceivedMatrixX][iterateoverReceivedMatrixY] = matrixOfSpotsReceived[iterateoverReceivedMatrixX][iterateoverReceivedMatrixY];//copy the matrix segment
		
		receiveCount++;
    }
   
    //draw the game board
    int xiter, yiter;
    glColor3f(0.00,0.00,0.00);
    for(yiter = 0; yiter < YNUMGRID; yiter++)
		for(xiter = 0; xiter < XNUMGRID; xiter++)
		{
			if(matrixOfSpots[xiter][yiter] == 'a')
			{
				glColor3f(0.00,0.00,0.00);
				glRectf((double)xiter * 2 / XNUMGRID - 1.0, (double)yiter * 2 / YNUMGRID - 1.0, 2 * (xiter + 1.0) / XNUMGRID - 1.0, 2 * (yiter + 1.0) / YNUMGRID - 1.0);
			}
		}
	
    for(iterateOverNodes = 1; iterateOverNodes < size; iterateOverNodes++)
    {
	    xyToCheck[1] = -2;//signal to reset board
	    MPI_Send(&xyToCheck, 3, MPI_INT, iterateOverNodes, tag, MPI_COMM_WORLD);//this only tells the node to calculate
    }
	
    //gl finish stuff
    glutSwapBuffers();
}

void mouse(int button,int state,int xscr,int yscr)
{
	if(button==GLUT_LEFT_BUTTON)
		if(state==GLUT_DOWN)
		{
		    //while(1==1)
		    display();
		}
	if(button==GLUT_RIGHT_BUTTON)
	    if(state==GLUT_DOWN)
	    {
			int count;
			for(count = 0; count < 15; count++)
				display();
	    }
}

void keyin(unsigned char key,int xscr,int yscr)
{
	if(key=='q')
	{
		exit(0);
	}
}

void reshape(int wscr,int hscr)
{
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc,&argv);        
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    
    
    if(rank == 0)//head node
    {
		//gl setup
		glutInit(&argc,argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
		glutInitWindowSize(w,h);
		glutInitWindowPosition(100,50);
		glutCreateWindow("conway");
		
		glClearColor(1.0,1.0,1.0,0.0);
		glShadeModel(GL_SMOOTH);
		
		glutDisplayFunc(display);
		glutIdleFunc(NULL);
		glutKeyboardFunc(keyin);
		glutReshapeFunc(reshape);
		glutMouseFunc(mouse);
		
		//initialize matrix as all daed cells
		int iterX, iterY;
		for(iterY = 0; iterY < YNUMGRID; iterY++)
			for(iterX = 0; iterX < XNUMGRID; iterX++)
			matrixOfSpots[iterX][iterY] = 'd';
			
		//read input
		FILE	*fin;
		fin = fopen("glidergun.txt" ,"r");
		int n;
		char ch;
		if(fin==NULL) 
		{
			printf("No such file\n");
			return 1;
		}
		
		int currentX =5;
		int currentY = 5;
		
		while(1)//read loop
		{
			n=fread(&ch, sizeof(char), 1, fin);
			
			if(ch == '.')
			{
				matrixOfSpots[currentX][currentY] = 'd';
			}
			else if(ch == 'O')
			{
				matrixOfSpots[currentX][currentY] = 'a';
			}
				
			if(n==0)
				break;
					
			currentX++;
			
			if(ch=='\n')
			{
				currentX = 5;
				currentY++;
			}
		}
		
		fclose(fin);
		printf("done reading from file!\n");
		
		int iterateOverNodes;
		for(iterateOverNodes = 1; iterateOverNodes < size; iterateOverNodes++)
			MPI_Send(&matrixOfSpots, XNUMGRID * YNUMGRID, MPI_CHAR, iterateOverNodes, tag, MPI_COMM_WORLD);
		
		//start!
		glutMainLoop();
    }
    else
    {
		int rangeMin = (double)XNUMGRID * (double)(rank - 1) / (double)(size - 1);
		int rangeMax = (double)XNUMGRID * (double)(rank) / (double)(size - 1);
		printf("Rank: %d\tmin: %d\tmax: %d\n", rank, rangeMin, rangeMax);
		
		MPI_Recv(&matrixOfSpots, XNUMGRID * YNUMGRID, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &status);//get original board
		int xiter, yiter;
		for(xiter = 0; xiter < XNUMGRID; xiter++)		//update the new matrix board too
			for(yiter = 0; yiter < YNUMGRID; yiter++)
			newMatrixOfSpots[xiter][yiter] = matrixOfSpots[xiter][yiter];
		
		
		while(1)
		{
			MPI_Recv(&xyToCheck, 3, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);//get sent source
			int rankOfReceive = xyToCheck[0];
			if(rankOfReceive == 0 && xyToCheck[1] == -1)//calculate all nodes
			{
				xyToCheck[0] = rank;
				
				//an array with all of the points that changed
				int changedPoints[YNUMGRID * XNUMGRID / (size - 1)];//in this array even points are x (0, 2, 4, 6...) odd points are y (1, 3, 5, 7...)
				int changedPointsIterator = 0;
				
				//iterate through matrix to find cells that need to switch
				int iterX, iterY;
				for(iterY = 0; iterY < YNUMGRID; iterY++)
					for(iterX = rangeMin; iterX < rangeMax; iterX++)
					{
					int aliveNeighborCount = 0;
					char newState;
					
					
					int iterXX, iterYY;
					for(iterXX = iterX - 1; iterXX <= iterX + 1; iterXX++)
					{
						for(iterYY = iterY - 1; iterYY <= iterY + 1; iterYY++)
						{
							if(iterXX > 0 && iterYY > 0 && iterXX < XNUMGRID && iterYY < YNUMGRID)
							{
								if(!(iterXX == iterX && iterYY == iterY))
								{
									if(iterXX < rangeMin)//send to node to the left
									{
										xyToCheck[1] = iterXX;
										xyToCheck[2] = iterYY;
										MPI_Send(&xyToCheck, 3, MPI_INT, rank - 1, tag, MPI_COMM_WORLD);
										MPI_Recv(&xyToCheck, 3, MPI_INT, rank - 1, tag, MPI_COMM_WORLD, &status);
										if(xyToCheck[1] == 1)//Its alive!
										{
											aliveNeighborCount++;
										}
									}
									else if(iterXX > (rangeMax - 1))//send to node to the right
									{
										xyToCheck[1] = iterXX;
										xyToCheck[2] = iterYY;
										MPI_Send(&xyToCheck, 3, MPI_INT, rank + 1, tag, MPI_COMM_WORLD);
										MPI_Recv(&xyToCheck, 3, MPI_INT, rank + 1, tag, MPI_COMM_WORLD, &status);
										if(xyToCheck[1] == 1)//Its alive!
										{
											aliveNeighborCount++;
										}
									}
									else
									{
										if(matrixOfSpots[iterXX][iterYY] == 'a')
										aliveNeighborCount++;
									}
								}
							}
						}
					}
					
					//these only set markers for what to change
					if(matrixOfSpots[iterX][iterY] == 'a' && (aliveNeighborCount != 2 && aliveNeighborCount != 3))
					{
						changedPoints[changedPointsIterator * 2] = iterX;
						changedPoints[changedPointsIterator * 2 + 1] = iterY;
						changedPointsIterator++;
					}
					else if(matrixOfSpots[iterX][iterY] == 'd' && aliveNeighborCount == 3)
					{
						changedPoints[changedPointsIterator * 2] = iterX;
						changedPoints[changedPointsIterator * 2 + 1] = iterY;
						changedPointsIterator++;
					}
				}
			   
			//we actually change the points here
			int iterateOverChangedPoints;
			for(iterateOverChangedPoints = 0; iterateOverChangedPoints < changedPointsIterator; iterateOverChangedPoints++)
			{
				int xToChange = changedPoints[iterateOverChangedPoints * 2];
				int yToChange = changedPoints[iterateOverChangedPoints * 2 + 1];
				if(matrixOfSpots[xToChange][yToChange] == 'a')
					newMatrixOfSpots[xToChange][yToChange] = 'd';
				else
					newMatrixOfSpots[xToChange][yToChange] = 'a';
			}
			MPI_Send(&rank, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
			MPI_Send(&newMatrixOfSpots, XNUMGRID * YNUMGRID, MPI_CHAR, 0, tag, MPI_COMM_WORLD);//sends the matrix back
			}
			else if(rankOfReceive == 0 && xyToCheck[1] == -2)//reset all nodes
			{
				for(xiter = 0; xiter < XNUMGRID; xiter++)		//reset the matrix with new spots
					for(yiter = 0; yiter < YNUMGRID; yiter++)
						matrixOfSpots[xiter][yiter] = newMatrixOfSpots[xiter][yiter];
			}
			else	//send back the state of a coordinate
			{
				if(matrixOfSpots[xyToCheck[1]][xyToCheck[2]] == 'd')
				{
					xyToCheck[1] = 0;
				}
				else
				{
					xyToCheck[1] = 1;
				}
				MPI_Send(&xyToCheck, 3, MPI_INT, rankOfReceive, tag, MPI_COMM_WORLD);
			}
		}
    }
}