#!/usr/bin/env python2.7
'''
    sends heartbeats at specific particular intervals
'''
import socket
import sys
from pisettings import PI_SETTINGS
from time import sleep
import psutil


def createSocket():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print('socket created...')
        return s
    except socket.error, msg:
        print('Socket init failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit();


def doHeartbeats():
    sock = createSocket()
    headNode = PI_SETTINGS["CLUSTER"]["HEAD_HOSTNAME"]
    PORT = int(PI_SETTINGS["CLUSTER"]["UDP_PORT"])
    heartbeatInterval = float(PI_SETTINGS["CLUSTER"]["HEARTBEAT_INTERVAL_MS"])

    while True:
        cpu = psutil.cpu_percent(interval=heartbeatInterval)
        #cpu = psutil.cpu_percent()
        sock.sendto(str(int(cpu)), (headNode, PORT))
        #sleep(heartbeatInterval)
        print("sending heartbeat")


def main():
    doHeartbeats()


if __name__ == '__main__':
    main()
