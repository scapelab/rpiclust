#!/usr/bin/env python2.7
'''
  Moves the local servo to a spacific angle (0-100) where 100 is furthest out
  and 0 is the furthest in (usually considered the default position)
'''
import argparse
import os
from pisettings import PI_SETTINGS


def getDistanceInPWM(distance):
    if distance is None or distance < 0 or distance > 100:
        print("Error: incorrect distance \n usage: moveservo [distance (0-100)]")
        return

    #get the pwm properties of this setup
    pwmMin = int(PI_SETTINGS["PIGPIO"]["PIGS_PWM_MIN"])
    pwmMax = int(PI_SETTINGS["PIGPIO"]["PIGS_PWM_MAX"])

    #got to inverse the distance as PWM is inversed
    distance = 100 - distance

    diff = pwmMax - pwmMin
    conversion = diff / 100;
    return (distance * conversion) + pwmMin


def doMove(d):
  distance = int(d)
  pigsPath = PI_SETTINGS["PIGPIO"]["PIGS_INTERFACE_PATH"]
  pin = PI_SETTINGS["PI"]["SERVO_PIN"]
  pwm = getDistanceInPWM(distance)
  #print(pigsPath)
  #print(pin)
  #print(distance + ": from inside doMove did pwm: " + str(pwm))
  #subprocess.call([pigsPath, 'PWM', pin, str(pwm)])
  os.system(" ".join([pigsPath, 'PWM', pin, str(pwm)]))


def main():
    parser = argparse.ArgumentParser(prog='moveservo', usage=' %(prog)s [distance (0-100)]')
    parser.add_argument('distance', type=int)
    cmdlnArgs = parser.parse_args()

    print(cmdlnArgs.distance)
    doMove(cmdlnArgs.distance)

if __name__ == '__main__':
    main()
