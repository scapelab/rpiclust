#!/usr/bin/env python2.7
'''
    Listens to the network socket for udp packets and reacts appropriatley
'''
import argparse
import socket
import sys
import os
import mmap
import thread
import threading
import subprocess
import shlex
from time import sleep
from pisettings import PI_SETTINGS
from moveservo import doMove

def createSocket():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print('socket created...')
        host = ''
        port = int(PI_SETTINGS["CLUSTER"]["UDP_PORT"])
        s.bind((host, port))
        return s
    except socket.error, msg:
        print('Socket init failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit();

def createStatusMmap():
    # open a file to use for Pi statuses
    statusFile = PI_SETTINGS["CLUSTER"]["MASTER_STATUS_FILE"]
    fd = os.open(statusFile, os.O_CREAT | os.O_TRUNC | os.O_RDWR)

    # resize the file and zero it out
    numPis = int(PI_SETTINGS["CLUSTER"]["NUM_PIS"])
    statusSize = int(PI_SETTINGS["CLUSTER"]["SINGLE_PI_STATUS_SIZE"])
    #size of the status file contains status for each pi (plus add the new line byte)
    statusFileSize = (statusSize+1) * numPis
    #assert os.write(fd, '\x00' * statusFileSize) == statusFileSize
    assert os.write(fd, os.linesep * statusFileSize) == statusFileSize


    # create the mmap
    buf = mmap.mmap(fd, statusFileSize, mmap.MAP_SHARED, mmap.PROT_WRITE)
    for i in range(numPis):
        buf.seek(i*(statusSize+1))
        buf.write('000')
    return buf



def listenAsMaster():
    sock = createSocket()
    buf = createStatusMmap()
    statusSize = int(PI_SETTINGS["CLUSTER"]["SINGLE_PI_STATUS_SIZE"])

    while True:
        data, addr = sock.recvfrom(64)
        cmdList = data.split()
        sourceId = int(addr[0].split('.')[3])
        print('From: ' + str(sourceId))
        cpuUsage = "%03d" % int(data)
        print('Message received: ' + cpuUsage)
        buf.seek((sourceId-1)*(statusSize+1))
        buf.write(cpuUsage)


servoDistance = 0
e = threading.Event()
p_readcpu = None

def readContinuously(threadName, sock):
    print('started background socket thread')
    global servoDistance
    global p_readcpu
    while True:
        data, addr = sock.recvfrom(64)
        cmdList = data.split()
        #print('Message received: ' + data)
        if cmdList[0] == 'moveservo':
            servoDistance = int(cmdList[1])
            e.set()
        elif cmdList[0] == 'sleep':
           print('sleeping')
           sleep(int(cmdList[1]))
        elif cmdList[0] == 'reboot':
           print('rebooting this machine')
           subprocess.call('reboot')
        elif cmdList[0] == 'readcpu':
          if p_readcpu is None:
            print('starting cpu reader')
            args = ['/home/bm/rpiclust/bin/readcpu/readcpu.pl', '5', '-s']
            p_readcpu = subprocess.Popen(args)
          else:
            print('stopping cpu reader')
            p_readcpu.terminate()
            p_readcpu = None

def listenAsSlave():
    global servoDistance
    sock = createSocket()
    e.clear()
    try:
        thread.start_new_thread(readContinuously, ("StartReader", sock, ))
        localServoDistance = servoDistance
        while True:
            if localServoDistance != servoDistance:
                print(str(servoDistance))
                localServoDistance = servoDistance
                doMove(localServoDistance)
            else:
                e.clear()
                e.wait()

    except Exception, errtxt:
        print(errtxt)
        print("Error creating thread")

def main():
    parser = argparse.ArgumentParser(prog='netlistener',
                                     description='Pi Cluster udp packet listerner')
    parser.add_argument('-m', dest='isMaster', action='store_true',
                        help='Listens to the network as master of the cluster',
                        required=False)
    cmdlnArgs = parser.parse_args()

    if cmdlnArgs.isMaster:
        print("Hello Master. ")
        listenAsMaster();
        return

    print("Hello Slave.")
    listenAsSlave();


if __name__ == '__main__':
    main()
