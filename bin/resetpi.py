#!/usr/bin/env python2.7
'''
  Resets any Pi based on a passed x,y coordinate to its original position
'''
import argparse
import subprocess

#TODO: add the ability to send a scpeific pi id number if the user already has it
def resetpi(row, col):
    print("resetting %s , %s" %(row, col))

    from coordstoid import getId
    id = getId(row, col)
    #id = subprocess.call(['coordstoid', str(row), str(col)])

    if id != -1:
      from sendcmd2pi import sendCmd
      cmd = 'moveservo.py ' + '0'
      print (str(id) + " " + cmd)
      sendCmd(id, cmd)

def main():
    parser = argparse.ArgumentParser(prog='resetpi', usage=' %(prog)s [row] [col]')
    parser.add_argument('row', help='row of the target pi', type=int);
    parser.add_argument('col', help='column of the target pi', type=int);

    cmdlnArgs = parser.parse_args()

    if cmdlnArgs.row < 0 or cmdlnArgs.col < 0:
      print("Error: row and column arguments should be greater than 0")
      return

    resetpi(cmdlnArgs.row, cmdlnArgs.col)

if __name__ == '__main__':
    main()
